// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Делегат для текстового поля ячейки

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/// Делегат для текстового поля ячейки
@interface TVFTextFieldDelegate : NSObject<UITextFieldDelegate>

@end
