// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: класс хранения данных для TableView

#import <Foundation/Foundation.h>

/// Хранилище данных, отображаемых в TableView
@protocol TVFDataObject

/// Хранимая строка
@property (strong) NSString* value;

/// Номер отображаемой картинки
@property (assign) NSInteger image;

@end
