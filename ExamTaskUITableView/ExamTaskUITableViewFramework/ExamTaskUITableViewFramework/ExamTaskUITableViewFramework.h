// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: Стандартный сгенерированный заголовочный файл

#import <UIKit/UIKit.h>

//! Project version number for ExamTaskUITableViewFramework.
FOUNDATION_EXPORT double ExamTaskUITableViewFrameworkVersionNumber;

//! Project version string for ExamTaskUITableViewFramework.
FOUNDATION_EXPORT const unsigned char ExamTaskUITableViewFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ExamTaskUITableViewFramework/PublicHeader.h>
#import <ExamTaskUITableViewFramework/Cell.h>
#import <ExamTaskUITableViewFramework/DataObject.h>
