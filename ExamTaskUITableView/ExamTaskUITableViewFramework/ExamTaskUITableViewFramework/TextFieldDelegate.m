// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Делегат для текстового поля ячейки

#import "TextFieldDelegate.h"

@implementation TVFTextFieldDelegate

// Обработка нажатия Enter
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField endEditing:YES];
	return YES;
}

@end
