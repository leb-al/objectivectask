// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: View-Controller для ячейки таблицы

#import <UIKit/UIKit.h>
#import "DataObject.h"

/// Делегат для ячейки таблицы
@protocol TVFCellDelegate
@required
/// Удалить ячейку
-(void)deleteCell:(id)cell;
/// Вставить ячейку после данной
-(void)insertAfter:(id)cell;
/// На экране появились кнопки добавления/удаления ячейки
-(void)controlsInvokedIn:(id)cell;

@end

#pragma mark -

/// View для ячейки
@interface TVFCell : UITableViewCell

/// Хранимые данные
@property (strong) id<TVFDataObject> value;
/// Делегат
@property (strong) NSObject<TVFCellDelegate>* delegate;

/// Скрыть кнопки добавления/удаления ячейки
-(void)hideControls;

@end


