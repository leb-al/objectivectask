// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: View-Controller для ячейки таблицы. Реализация

#import "Cell.h"
#import "TextFieldDelegate.h"

/// Ширина кнопок добавить и удалить
static const int insertDeleteButtonWidht = 22;

///View для ячейки
@interface TVFCell ()
/// Картинка
@property (weak, nonatomic) IBOutlet UIImageView* image;
/// Текстовое поле
@property (weak) IBOutlet UITextField* textEditField;
/// Делагат текстовго поля
@property (strong) TVFTextFieldDelegate* textDelegate;
/// Кнопка добавления ячейки
@property (weak, nonatomic) IBOutlet UIButton* insertCellButton;
/// Кнопка удаления ячейки
@property (weak, nonatomic) IBOutlet UIButton* deleteCellButton;
/// Ограничение на ширину кнопки добавления ячейки
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* insertButtonWidth;
/// Ограничение на ширину кнопки удаления ячейки
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* deleteButtonWidth;
@end

@implementation TVFCell {
	id<TVFDataObject> _value;
}


- (id<TVFDataObject>)value
{
	return _value;
}

- (void)setValue:(id<TVFDataObject>)valueObject
{
	_value = valueObject;
	
	_textEditField.text = valueObject.value;
	if(_textDelegate == nil) {
		self.textDelegate = [TVFTextFieldDelegate new];
	}
	_textEditField.delegate = _textDelegate;
	TVFCell* _self = self;
	dispatch_async(dispatch_get_main_queue(), ^{
		NSString* name = [NSString stringWithFormat:@"img%d", valueObject.image];
		UIImage* image = [UIImage imageNamed:name inBundle:[NSBundle bundleForClass:[TVFCell class]]
			   compatibleWithTraitCollection:nil];
		_self.image.image = image;
	});
}

- (IBAction)textChanged:(id)sender
{
	_value.value = _textEditField.text;
}

- (IBAction)swipeRight:(id)sender
{
	self.insertCellButton.hidden = NO;
	self.insertButtonWidth.constant = insertDeleteButtonWidht;
	[self hideDeleteButton];
	if( self.delegate != nil ) {
		[self.delegate controlsInvokedIn:self];
	}
}

- (IBAction)swipeLeft:(id)sender
{
	self.deleteCellButton.hidden = NO;
	self.deleteButtonWidth.constant = insertDeleteButtonWidht;
	[self hideInsertButton];
	if( self.delegate != nil ) {
		[self.delegate controlsInvokedIn:self];
	}
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	UISwipeGestureRecognizer* rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
	rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
	[self addGestureRecognizer:rightRecognizer];
	UISwipeGestureRecognizer* leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
	leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
	[self addGestureRecognizer:leftRecognizer];
}

- (IBAction)insertButtonPressed:(id)sender
{
	[self hideInsertButton];
	if( self.delegate != nil ) {
		[self.delegate insertAfter:self];
	}
}

- (IBAction)deleteButtonPressed:(id)sender
{
	[self hideDeleteButton];
	if( self.delegate != nil ) {
		[self.delegate deleteCell:self];
	}
}

- (void)hideInsertButton
{
	self.insertCellButton.hidden = YES;
	self.insertButtonWidth.constant = 0;
}

- (void)hideDeleteButton
{
	self.deleteCellButton.hidden = YES;
	self.deleteButtonWidth.constant = 0;
}

- (void)hideControls
{
	[self hideDeleteButton];
	[self hideInsertButton];
}
@end
