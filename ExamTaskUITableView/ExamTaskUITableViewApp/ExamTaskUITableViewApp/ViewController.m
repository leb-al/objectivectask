// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: ViewController для главного окна

#import <ExamTaskUITableViewFramework/Cell.h>
#import "ViewController.h"
#import "TableDataSource.h"
#import "SectionView.h"

/// Высота ячейки таблицы
const int rowHeight = 50;
/// Высота заголовка секции
const int sectionHeight = 60;

/// View-Controller для экрана с UITableView
@interface TVAViewController () {
@private
	TVATableDataSource* _dataSource;
}

@property (weak, nonatomic) IBOutlet UITableView* table;

@end

@implementation TVAViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	id delegate = [[UIApplication sharedApplication] delegate];
	
	_dataSource = [TVATableDataSource new];
	_dataSource.cellDelegate = self;
	_dataSource.managedObjectContext = [delegate managedObjectContext];
	
	_table.dataSource = _dataSource;
	_table.delegate = self;
	[_table registerNib:[UINib nibWithNibName:@"Cell" bundle:[NSBundle bundleWithIdentifier:
		@"com.abbyy.ExamTaskUITableViewFramework"]] forCellReuseIdentifier:cellViewName];
	
	UINib* headerNib = [UINib nibWithNibName:@"TableHeaderView" bundle:nil];
	TVATableHeaderView* headerView  = [[headerNib instantiateWithOwner:nil options:nil] objectAtIndex:0];
	headerView.delegate = self;
	_table.tableHeaderView = headerView;
}

#pragma mark -

- (void)enterReorderingMode
{
	[_table setEditing:YES animated:YES];
}

- (void)leaveReorderingMode
{
	[_table setEditing:NO];
}

// Этот метод написан для того, чтобы клавиатура убиралась при выборе другой ячейки
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
	[_table endEditing:YES];
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
	return rowHeight;
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
	UINib* nib = [UINib nibWithNibName:@"SectionView" bundle:nil];
	TVASectionView* view = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
	view.section = [_dataSource getSectionAtIndex:section];
	view.dataSource = _dataSource;
	return view;
}

- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
	return sectionHeight;
}

// Отключаем стандартные контролы удаления ячеек
- (UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath
{
	if (self.table.editing)
	{
		return UITableViewCellEditingStyleDelete;
	}
	
	return UITableViewCellEditingStyleNone;
}

#pragma mark -

- (void)addSection
{
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Section adding", nil)  message:
	  NSLocalizedString(@"Enter section header", nil) delegate:self cancelButtonTitle: NSLocalizedString(@"Cansel", nil) otherButtonTitles:
		NSLocalizedString(@"Ok", nil), nil];
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	[alert show];
}

- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex == 1) {
		[_dataSource addSection:[alertView textFieldAtIndex:0].text for:_table];
	}
}

#pragma mark -

-(void)controlsInvokedIn:(id)cell
{
	for(id currentCell in self.table.visibleCells) {
		// Второе условие, т.к. в режиме редактирования контролы запрещены
		if(currentCell != cell || self.table.editing) {
			[currentCell hideControls];
		}
	}
}

-(void)deleteCell:(id)cell
{
	NSIndexPath* indexPath = [self.table indexPathForCell:cell];
	[_dataSource deleteCellAt:indexPath];
	[self.table deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)insertAfter:(id)cell
{
	NSIndexPath* indexPath = [self.table indexPathForCell:cell];
	// Сдвигаем на следующую строчку
	indexPath = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
	[_dataSource insertCellAt:indexPath];
	[self.table insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
	
@end
