// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: View для заголовка секции таблицы


#import <UIKit/UIKit.h>
#import "TableDataSource.h"

/// Заголовк секции таблицы
@interface TVASectionView : UIView

/// Имя секции
@property (strong, nonatomic) id<TVASection> section;

/// TableView для секции
@property (weak, nonatomic) TVATableDataSource* dataSource;
@end
