// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: Заголовок таблицы

#import <UIKit/UIKit.h>

/// Протокол для делегата заголовка
@protocol TVATableHeaderDelegate <NSObject>

@required
/// Вошли врежим редактирования
- (void)enterReorderingMode;

/// Вышли из режима редактирования
- (void)leaveReorderingMode;

/// Нажата кнопка добавления секции
- (void)addSection;

@end

#pragma mark -
//---------------------------------

/// Класс вьюшки для заголовка
@interface TVATableHeaderView : UIView
@property (strong) NSObject<TVATableHeaderDelegate>* delegate;
@end
