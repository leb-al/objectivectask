// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: View для заголовка секции таблицы. Реализация

#import <ExamTaskUITableViewFramework/DataObject.h>
#import "SectionView.h"

@interface TVASectionView ()

@property (strong, nonatomic) IBOutlet UILabel* headerLabel;

@end

@implementation TVASectionView {
	id<TVASection> _section;
}

- (id<TVASection>)section
{
	return _section;
}

- (void)setSection:(id<TVASection>)section
{
	_section = section;
	self.headerLabel.text = section.header;
	
}

- (IBAction)addButtonTouched:(id)sender {
	[self.dataSource insertCellInRow:self.section.items.count forSection:self.section];
	UITableView* tableView = (UITableView*)self.superview;
	[tableView reloadData];
}

@end
