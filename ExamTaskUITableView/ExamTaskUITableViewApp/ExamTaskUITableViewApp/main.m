//
//  main.m
//  ExamTaskUITableViewApp
//
//  Created by Aleksey Lebedev on 19/12/2016.
//  Copyright © 2016 Abbyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([TVAAppDelegate class]));
	}
}
