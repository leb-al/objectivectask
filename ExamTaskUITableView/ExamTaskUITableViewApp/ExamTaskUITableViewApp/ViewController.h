// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: View-Controller для главного окна

#import <UIKit/UIKit.h>
#import <ExamTaskUITableViewFramework/Cell.h>
#import "TableHeaderView.h"

@interface TVAViewController : UIViewController<TVATableHeaderDelegate, UITableViewDelegate, UIAlertViewDelegate, TVFCellDelegate>

@end


