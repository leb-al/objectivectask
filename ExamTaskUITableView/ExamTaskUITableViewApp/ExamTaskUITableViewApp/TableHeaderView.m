// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: Заголовок таблицы

#import "TableHeaderView.h"

@interface TVATableHeaderView ()
- (IBAction)switchValueChanged:(id)sender;
- (IBAction)addSectionButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *reorderingSwitch;
@end

@implementation TVATableHeaderView


- (IBAction)switchValueChanged:(id)sender {
	if([sender isOn]) {
		[_delegate enterReorderingMode];
	} else {
		[_delegate leaveReorderingMode];
	}
}

- (IBAction)addSectionButtonPressed:(id)sender {
	[_delegate addSection];
}
@end
