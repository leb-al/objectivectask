// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: View-Controller для главного окна

#import "AppDelegate.h"

@interface TVAAppDelegate ()

@end

@implementation TVAAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
	return YES;
}

- (void)applicationDidEnterBackground:(UIApplication*)application
{
	[self saveContext];
}

- (NSManagedObjectModel*)managedObjectModel
{
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	
	NSURL* modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	
	return _managedObjectModel;
}

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator
{
	if(_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	NSURL* storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TableViewApp.sqlite"];
	
	NSError* error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
	if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error: &error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
	
	return _persistentStoreCoordinator;
}

- (NSManagedObjectContext*)managedObjectContext
{
	if(_managedObjectContext != nil) {
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator* coordinator = [self persistentStoreCoordinator];
	if(coordinator != nil) {
		_managedObjectContext = [[NSManagedObjectContext alloc] init];
		[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	
	return _managedObjectContext;
}

- (NSURL*)applicationDocumentsDirectory
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext
{
	NSError* error = nil;
	NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
	
	if(managedObjectContext != nil) {
		if([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			NSLog(@"Error saving context %@, %@", error, [error userInfo]);
			abort();
		}
	}
}

@end
