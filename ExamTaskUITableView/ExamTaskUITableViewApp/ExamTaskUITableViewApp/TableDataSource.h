// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: источник данных для таблицы

#import <Foundation/Foundation.h>
#import <ExamTaskUITableViewFramework/Cell.h>
#import <UIKit/UIKit.h>

/// Имя константы, определяющей view для ячеек
extern NSString* const cellViewName;

/// Класс, содержащий данные секции
@protocol TVASection

/// Список элементов
@property (strong, nonatomic) NSOrderedSet* items;
/// Заголовок секции
@property (strong, nonatomic) NSString* header;

@end

#pragma mark -

/// Источник данных для TableView
@interface TVATableDataSource : NSObject<UITableViewDataSource>

/// Делегат для ячеек таблицы
@property (weak, nonatomic) NSObject<TVFCellDelegate>* cellDelegate;

/// Контекст CoreData
@property (nonatomic, strong) NSManagedObjectContext* managedObjectContext;

/// Создание и вставка ячейки
/// @param row индекс, по которому вставить ячейку
/// @param group секция
- (void)insertCellInRow:(NSInteger)row forSection:(id<TVASection>)group;

/// Создание и вставка ячейки
/// @param indexPath индекс, по которому вставить ячейку
- (void)insertCellAt:(NSIndexPath*)indexPath;

/// Удаление ячейки
/// @param indexPath индекс, по которому удалить ячейку
- (void)deleteCellAt:(NSIndexPath*)indexPath;

/// Добавление секции
/// @param header Заголовок секции
- (void)addSection:(NSString*) header for:(UITableView*) tableView;

/// Получение секции по индексу
- (id<TVASection>)getSectionAtIndex:(NSInteger)index;

@end
