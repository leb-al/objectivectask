// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Алексей Лебедев
// Описание: источник данных для таблицы

#import <stdlib.h>

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <ExamTaskUITableViewFramework/DataObject.h>
#import <ExamTaskUITableViewFramework/Cell.h>
#import "TableDataSource.h"

/// Имя константы, определяющей view для ячеек
NSString* const cellViewName = @"TVFCell";

/// Название сущности секции в модели CoreData
NSString* const sectionEntityName = @"Section";

/// Название сущности секции в модели CoreData
NSString* const dataEntityName = @"DataObject";

/// Название свойства "имя" секции в модели CoreData
NSString* const sectionHeader = @"header";


///// Название сущности секции в модели CoreData
//NSString* const sectionsRequestCacheName = @"sectionsCache";

/// Количество достпных картинок
const int imageCount = 3;

// При создании TVATableDataSource генерируется случайное число элементов
/// Маскимальное число элеметнов в секции
const int maxItemsCountInSection = 7;
/// Маскимальное число элеметнов в секции
const int minItemsCountInSection = 2;

/// Число секций по-умолчанию
const int sectionsCount = 3;

#pragma mark -

@interface TVATableDataSource()
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;

@end

@implementation TVATableDataSource
- (instancetype)init
{
	self = [super init];
	return self;
}

-(NSFetchedResultsController*)fetchedResultsController
{
	if(_fetchedResultsController == nil) {
		NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
		NSEntityDescription* entity = [NSEntityDescription entityForName:sectionEntityName inManagedObjectContext:self.managedObjectContext];
		[fetchRequest setEntity:entity];
		NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sectionHeader ascending:YES];
		NSArray* sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
		[fetchRequest setSortDescriptors:sortDescriptors];
		NSFetchedResultsController* controller = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
																					 managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
		self.fetchedResultsController = controller;
		[self perfomeRequest];
		
		// Скорее всего, первый запуск. Сгенерируем данные
		if([self numberOfSectionsInTableView:nil]==0) {
			for(int j = 0; j < sectionsCount; ++j) {
				[self addSection:[NSString stringWithFormat:NSLocalizedString(@"Section #%d", nil), j] for:nil];
				int itemsCount = minItemsCountInSection + arc4random_uniform(maxItemsCountInSection - minItemsCountInSection);
				for(int i = 0; i < itemsCount; ++i) {
					NSIndexPath* indexPath = [NSIndexPath indexPathForItem:i inSection:j];
					[self insertCellAt:indexPath];
					id<TVFDataObject> dataObj = [self objectForPath:indexPath];
					dataObj.value = [NSString stringWithFormat:NSLocalizedString(@"Item #%d s %d", nil), i, j];
				}
			}
		}
	}
	return _fetchedResultsController;
}

-(void)perfomeRequest
{
	NSError* error = nil;
	if(![self.fetchedResultsController performFetch:&error]) {
		NSLog(@"Unresolved error loading data %@, %@", error, [error userInfo]);
		abort();
	}
}

-(void)saveContext:(BOOL)needReload
{
	NSError* error = nil;
	if(![self.managedObjectContext save:&error]) {
		NSLog(@"Saving error: %@, %@", error, [error userInfo]);
		abort();
	}
	if(needReload) {
		// перезагружаем данные
		[self perfomeRequest];
	}
}

#pragma mark -

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)index
{
	id<TVASection> section = self.fetchedResultsController.fetchedObjects[index];
	return section.items.count;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	id<TVASection> section = self.fetchedResultsController.fetchedObjects[indexPath.section];
	id<TVFDataObject> dataObject = section.items[indexPath.row];
	TVFCell* cell = [tableView dequeueReusableCellWithIdentifier:cellViewName];
	if(cell == nil) {
		cell  = [TVFCell new];
	}
	cell.delegate = self.cellDelegate;
	cell.value = dataObject;
	return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
	return self.fetchedResultsController.fetchedObjects.count;
}

- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
	id<TVASection> group = self.fetchedResultsController.fetchedObjects[section];
	return group.header;
}


- (void)tableView:(UITableView*)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
	switch (editingStyle) {
		case UITableViewCellEditingStyleDelete:
			[self deleteCellAt:indexPath];
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case UITableViewCellEditingStyleInsert:
			[self insertCellAt:indexPath];
			[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		default:
			NSAssert(NO, @"Not implimmented case in switch");
			break;
	}
}

- (BOOL)tableView:(UITableView*)tableView canEditRowAtIndexPath:(NSIndexPath*)indexPath
{
	return YES;
}


- (BOOL)tableView:(UITableView*)tableView canMoveRowAtIndexPath:(NSIndexPath*)indexPath
{
	return YES;
}

- (void)tableView:(UITableView*)tableView moveRowAtIndexPath:(NSIndexPath*)sourceIndexPath toIndexPath:(NSIndexPath*)destinationIndexPath
{
	id<TVASection> sourceSection = self.fetchedResultsController.fetchedObjects[sourceIndexPath.section];
	id<TVASection> destinationSection = self.fetchedResultsController.fetchedObjects[destinationIndexPath.section];
	id<TVFDataObject> dataObject = sourceSection.items[sourceIndexPath.row];
	NSMutableOrderedSet* sourceItems = [sourceSection.items mutableCopy];
	[sourceItems removeObjectAtIndex:sourceIndexPath.row];
	sourceSection.items = sourceItems;
	NSMutableOrderedSet* destinationItems = [destinationSection.items mutableCopy];
	[destinationItems insertObject:dataObject atIndex:destinationIndexPath.row];
	destinationSection.items = destinationItems;
	[self saveContext:YES];
}

#pragma mark -

- (id<TVFDataObject>)objectForPath:(NSIndexPath*)indexPath
{
	id<TVASection> group = [self getSectionAtIndex:indexPath.section];
	return group.items[indexPath.row];
}

- (void)insertCellInRow:(NSInteger)row forSection:(id<TVASection>)group
{
	NSManagedObjectContext* managedObjectContext = [self.fetchedResultsController managedObjectContext];
	NSManagedObject<TVFDataObject>* dataObj = [NSEntityDescription insertNewObjectForEntityForName:dataEntityName
															  inManagedObjectContext:managedObjectContext];
	dataObj.value = NSLocalizedString(@"New line", nil);
	dataObj.image = arc4random_uniform( imageCount );
	///[dataObj setValue:group forKey:@"section"];
	NSMutableOrderedSet* groupItems = [group.items mutableCopy];
	[groupItems insertObject:dataObj atIndex:row];
	group.items = groupItems;
	[self saveContext:YES];
}

- (void)insertCellAt:(NSIndexPath*)indexPath
{
	[self insertCellInRow:indexPath.row forSection:[self getSectionAtIndex:indexPath.section]];
}

- (void)deleteCellAt:(NSIndexPath*)indexPath
{
	[self.fetchedResultsController.managedObjectContext deleteObject:[self objectForPath:indexPath]];
	[self saveContext:YES];
}

- (void)addSection:(NSString*)header for:(UITableView*)tableView
{
	NSEntityDescription* entity = self.fetchedResultsController.fetchRequest.entity;
	NSManagedObject* newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:entity.name
		inManagedObjectContext: self.fetchedResultsController.managedObjectContext];
	[newManagedObject setValue:header forKey:sectionHeader];
	[self saveContext:YES];
	[tableView reloadData];
}

- (id<TVASection>)getSectionAtIndex:(NSInteger)index
{
	return self.fetchedResultsController.fetchedObjects[index];
}
@end
