// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: клиент
// Реализация контроллера для GUI

#import "ViewController.h"
#import "ExamTaskXpcServiceProtocol.h"
#import "AppDelegate.h"

@interface ViewController()
// Обработка нажатия кнопки
- (IBAction)onSearchButtonClick:(id)sender;

@end

@implementation ViewController
// Обработка нажатия кнопки
- (IBAction)onSearchButtonClick:(id)sender
{
	AppDelegate* delegate = [[NSApplication sharedApplication] delegate];
	NSXPCConnection* connection = delegate.connection;
	 __weak ViewController* _self = self;
	[[connection remoteObjectProxy] numberOfContactsWithQuery:self.searchField.stringValue withReply:^(NSUInteger result){
		NSLog(@"Result %u", (unsigned int)result);
		dispatch_async(dispatch_get_main_queue(), ^{
			[_self.resultsLabel setStringValue: [NSString stringWithFormat:@"Found: %u", (unsigned int)result]];
		});
	}];
}
@end
