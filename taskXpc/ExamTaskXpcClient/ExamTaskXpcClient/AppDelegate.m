// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: клиент

#import "AppDelegate.h"
#import "ExamTaskXpcServiceProtocol.h"

@interface AppDelegate ()
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	self.connection = [[NSXPCConnection alloc] initWithServiceName:@"com.abbyy.ExamTaskXpcService"];
	[self.connection setInterruptionHandler:^{
		NSLog(@"Interrupted");
	}];
	[self.connection setInvalidationHandler:^{
		NSLog(@"Invalidated");
	}];
	self.connection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(ExamTaskXpcServiceProtocol)];
	[self.connection resume];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	[self.connection invalidate];
}

@end
