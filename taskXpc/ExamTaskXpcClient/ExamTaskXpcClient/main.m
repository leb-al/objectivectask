//
//  main.m
//  ExamTaskXpcClient
//
//  Created by Aleksey Lebedev on 19/12/2016.
//  Copyright © 2016 Abbyy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
