// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: клиент

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
// соединение XPC
@property (strong) NSXPCConnection* connection;
@end

