// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: клиент

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property (strong) IBOutlet NSView *rootView;
@property (weak) IBOutlet NSTextField *searchField;
@property (weak) IBOutlet NSTextField *resultsLabel;

@end

