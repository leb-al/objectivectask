// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: XPC-сервис.
// Исполняемая часть.

#import <Foundation/Foundation.h>
#import "ExamTaskXpcService.h"

// Делегат для NSXPCListener
@interface ServiceDelegate : NSObject <NSXPCListenerDelegate>
@end

@implementation ServiceDelegate

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection
{
	newConnection.exportedInterface = [NSXPCInterface interfaceWithProtocol:@protocol(ExamTaskXpcServiceProtocol)];
	ExamTaskXpcService* exportedObject = [ExamTaskXpcService new];
	newConnection.exportedObject = exportedObject;
	[newConnection resume];
	return YES;
}

@end

int main(int argc, const char *argv[])
{
	ServiceDelegate* delegate = [ServiceDelegate new];
	NSXPCListener* listener = [NSXPCListener serviceListener];
	listener.delegate = delegate;
	[listener resume];
	return 0;
}
