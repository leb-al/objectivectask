// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: XPC-сервис.
// Реализация экспортируемого протокола

#import <AddressBook/AddressBook.h>
#import "ExamTaskXpcService.h"

// Реализация экспортируемого объекта
@implementation ExamTaskXpcService

-(void)numberOfContactsWithQuery:(NSString*)query withReply:(void (^)(NSUInteger))reply
{
	ABAddressBook* AB = [ABAddressBook sharedAddressBook];
	ABSearchElement* searchQuery =
	[ABPerson searchElementForProperty:kABFirstNameProperty label:nil key:nil value:query comparison:kABEqualCaseInsensitive];
	NSArray *peopleFound = [AB recordsMatchingSearchElement:searchQuery];
	reply([peopleFound count]);
}
@end
