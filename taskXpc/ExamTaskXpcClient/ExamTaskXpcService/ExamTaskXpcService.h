// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: XPC-сервис.
// Экспортируемый объект

#import <Foundation/Foundation.h>
#import "ExamTaskXpcServiceProtocol.h"

// Объект, реализующий экспортируемый протокол.
@interface ExamTaskXpcService : NSObject <ExamTaskXpcServiceProtocol>
@end
