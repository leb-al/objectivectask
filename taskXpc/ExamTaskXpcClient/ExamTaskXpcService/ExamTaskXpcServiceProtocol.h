// Copyright (С) ABBYY (BIT Software), 1993 - 2016. All rights reserved.
// Автор: Лебедев Алексей
// Описание: Задача по XPC для экзамена по Objective-C. Часть: XPC-сервис.
// Экспортируемый протокол

#import <Foundation/Foundation.h>

// Интерфейс для взаимодейстия с сервисом
@protocol ExamTaskXpcServiceProtocol

// Производит поиск людей по имени и возвращает количество совпадений
- (void)numberOfContactsWithQuery:(NSString *)query withReply:(void (^)(NSUInteger))reply;

@end
